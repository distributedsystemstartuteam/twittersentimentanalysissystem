import json
from kafka import SimpleProducer, KafkaClient
import tweepy
import configparser
import sys
class TweeterStreamListener(tweepy.StreamListener):
    def __init__(self, api,kafka_topic):
        self.api = api
    self.kafka_topic = kafka_topic
        super(tweepy.StreamListener, self).__init__()
        client = KafkaClient("mahernode.com.com:6667")
        self.producer = SimpleProducer(client, async = True,
                          batch_send_every_n = 1000,
                          batch_send_every_t = 10)

    def on_status(self, status):
        msg =  status.text.encode('utf-8')
        try:
            self.producer.send_messages(kafka_topic, msg)
        except Exception as e:
            print(e)
            return False
        return True

    def on_error(self, status_code):
        print("Error received in kafka producer")
        return True 

    def on_timeout(self):
        return True 

if __name__ == '__main__':

    # Assigine our API twitter credentilas
    consumer_key = '$$$'
    consumer_secret = '$$$'
    access_key = '$$$'
    access_secret = '$$$'
    
    kafka_topic = sys.argv[1]
    keyword_1 = sys.argv[2]
    keyword_2 = sys.argv[3]
    #print(kafka_topic)
    
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)

    stream = tweepy.Stream(auth, listener = TweeterStreamListener(api, kafka_topic))

    stream.filter(track = [keyword_1], languages = ['en'])