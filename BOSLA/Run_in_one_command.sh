#!/bin/bash
echo Start the parsing process

sh /root/parse.sh &


#create a new topic for kafka
echo Create Kafka Topic
cd /usr/hdp/3.0.1.0-187/kafka/
bin/kafka-topics.sh --create --zookeeper mkamel.com.com:2181 --replication-factor  1 --partitions 1 --topic $1

#submit spark job for getting tweets from twitter
echo Get Tweets Steam
cd /usr/hdp/3.0.1.0-187/spark2/

echo Start Analysis
./bin/spark-submit /root/gettweets_bkp.py $1 $2 &

./bin/spark-submit --jars /tmp/mozilla_root0/spark-streaming-kafka-0-8-assembly_2.11-2.4.0.jar /root/spark_bkp.py $1  > /root/$3 &

chmod 777 /root/sent.csv

echo Transfer Results to HDFS
hadoop fs -put /root/sent.csv /user