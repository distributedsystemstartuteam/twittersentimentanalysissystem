<?php
/**
 * Created by PhpStorm.
 * User: Said Kazimov
 * Date: 9/28/2018
 * Time: 5:24 PM
 */

namespace TwitterSentiment;


class TwitterSentDb
{
    private $pdo;

    public function __construct($pdo) {
        $this->pdo = $pdo;
    }
	public function run_py($keyword){
		$output = passthru('py C:\Users\Said Kazimov\Desktop\index.py aaa');
		echo $output;
	}
	
    public function all() {
        $stmt = $this->pdo->query('SELECT id, type, date, name,tweet '
            . 'FROM twitter_sentiment '
            . 'ORDER BY id');
        $tw_sent = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $tw_sent[] = [
                'id' => $row['id'],
                'type' => $row['type'],
                'date' => $row['date'],
                'name' => $row['name'],
                'tweet' => $row['tweet']
            ];
        }
        return $tw_sent;
    }
}

/**
 * Create table in PostgreSQL from PHP demo
 */
