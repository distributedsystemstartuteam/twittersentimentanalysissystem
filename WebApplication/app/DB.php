<?php

namespace TwitterSentiment;

require 'vendor/autoload.php';

use TwitterSentiment\Connection as Connection;
use TwitterSentiment\TwitterSentDb as TWSentDb;

class DB
{

    private static $conn;
    public static function get() {
        if (null === static::$conn) {
            static::$conn = new static();
        }

        return static::$conn;
    }
    public function connect() {
        try {
            // connect to the PostgreSQL database
            global $pdo;
            $pdo = Connection::get()->connect_mysql();

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }
}