# coding: utf-8
import csv


tot_pos = 0
tot_neg = 0
tot_neu = 0

import re
RE_EMOJI = re.compile('[\U00010000-\U0010ffff]', flags=re.UNICODE)


# In[3]:
pos_words = []
neg_words = []

# In[4]:
#Add English Lexicon words
with open('Hu and Liu Lexicon.csv') as csvfile:
    lexicon = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in lexicon:
        clas = row[1]
        row = row[0].split()
        if clas == '1':
            for r in row:
                pos_words.extend([r.strip()])
        else:
            for r in row:
                neg_words.extend([r.strip()])



#read negation words
negation_words = []
with open('negation_words.txt') as f:
    negation_words = f.read().splitlines() 


#read stop words
stop_words = []
def remove_stop(word_features2):
    with open('stop_words.txt') as f:
        stop_words = f.read().splitlines() 


#classify a comment
def comm_classifier(comm):
    comm = comm.lower()
    sum_words = 1e-9
    cn_value = 0
    neg = 1
    counter = 0
    comm = comm.split()
    for w in comm:
        counter += 1
        if w in stop_words: #pass the stop words
            continue
        elif w in negation_words:
            neg *= -1
            counter = 0
            continue
        if counter > 2:
            counter = 0
            neg = 1
        
        pos = pos_words.count(w)
        negi = neg_words.count(w)
        if pos > 0 or negi > 0:
            sum_words += 1
        
        if pos > negi:
            cn_value += (neg * (pos/(pos+negi)))
        elif negi > pos:
            cn_value += (neg * -(negi/(pos+negi)))
    
    val = cn_value/sum_words
    
    while 1:
        if val >= 0:
            val = 1
            break
        else:
            val = -1
            break    
            
    return val


print(comm_classifier('I hate this movie'))