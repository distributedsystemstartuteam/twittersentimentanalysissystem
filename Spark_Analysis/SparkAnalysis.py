from pyspark import SparkConf, SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql import HiveContext
from pyspark.sql import *
from pyspark.sql import Row
from pyspark.sql.types import *
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql import SparkSession
import operator
import numpy as np
import tkinter
import matplotlib.pyplot as plt
import sys

conf = SparkConf().setMaster("yarn").setAppName("Streamer")
sc = SparkContext(conf=conf)
hc = HiveContext(sc)
SparkContext.setSystemProperty("hive.metastore.uris","thrift://mkamel.com.com:9083")

def main(kafka_topic):
    ssc = StreamingContext(sc, 10)
    ssc.checkpoint("checkpoint")

    pwords = load_wordlist("positive.txt")
    nwords = load_wordlist("negative.txt")
   
    counts = stream(ssc, pwords, nwords, 100,kafka_topic)


def load_wordlist(filename):
    rdd = sc.textFile(filename)
    return set(rdd.collect())

def fx(word,pwords,nwords):
    if word in pwords:
        return "positive"
    elif word in nwords:
        return "negative"
    else:
        return None

def updateFunction(newValues, runningCount):
    if runningCount is None:
       runningCount = 0
    return sum(newValues, runningCount) 

def stream(ssc, pwords, nwords, duration, kafka_topic):
    kstream = KafkaUtils.createDirectStream(
        ssc, topics = [kafka_topic], kafkaParams = {"metadata.broker.list": 'mahernode.com.com:6667'})
    tweets = kstream.map(lambda x: x[1].encode("ascii","ignore"))
    words = tweets.flatMap(lambda line: line.split(" "))
    pairs = words.map(lambda word: (fx(word,pwords,nwords), 1)).filter(lambda x: x[0]=="positive" or x[0] == "negative")


    wordCounts = pairs.reduceByKey(lambda x, y: x + y)
    wordCounts.saveAsTextFiles('newdb/counts.txt')
    running_counts = pairs.updateStateByKey(updateFunction)
    running_counts.pprint()
    counts = []
    wordCounts.foreachRDD(lambda t,rdd: counts.append(rdd.collect()))
    ssc.start()                         # Start the computation
    ssc.awaitTerminationOrTimeout(duration)
    ssc.stop(stopGraceFully=True)
    return counts


if __name__=="__main__":
    kafka_topic = sys.argv[1]    
    main(kafka_topic)
