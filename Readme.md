# BOSLA: Distributed Twitter Sentiment Analysis System

## Team Members:
#### 1) Mahmoud Kamel Shoush
#### 2) Nesma Mahmoud
#### 3) Mohamed Maher
#### 4) Jamil Gurbanzade
#### 5) Said Kazimov

## Project Objectives and Goals:
#### - Real Time Sentiment Analysis For Twitter Stream Over a Distributed System

## Goals:
#### - Build a Distributed Cluster
#### - Ingest Tweets from Twitter
#### - Tweets Filtration
#### - Sentiment Analysis Over Stream
#### - Visualization

## Repository Content:
#### - BOSLA (Main Bash Scripts and Parsers Used in System)
#### - Services Configurations (Configuration Files of different Services using on Horton Works)
#### - Kafka Stream (Python Scripts for Twitter Data Ingestion)
#### - Web Application (Web page for System visualization)
#### - Spark Stream (Python Scripts for Analysis over twitter stream from kafka)
#### |
##### - - Lexicon Approach (Scripts for lexicon approach of sentiment analysis)
##### - - Model Approach (Scripts for ML Model approach of sentiment analysis)